using System.Collections.Generic;
using System.Linq;


namespace Valhalla.Validation
{
	public class ValidationResult
	{
		public readonly List<string> Errors = new List<string>();


		public bool IsInvalid
			=> Errors.Count > 0;


		public string GetCombinedError()
			=> Errors.Aggregate(
				"Error:",
				(current, next) => $"{current}\n\n{next}"
			);


		public void AddError(string message)
		{
			Errors.Add(message);
		}


		public void AddChildErrors(string name, IValidatable child)
		{
			var errors = child.Validate();

			if (!errors.IsInvalid)
				return;

			AddError($"{name}:");

			foreach (var message in errors.Errors)
				AddError($"\t{message}");
		}
	}
}
