

namespace Valhalla.Validation
{
	public interface IValidatable
	{
		ValidationResult Validate();
	}
}
