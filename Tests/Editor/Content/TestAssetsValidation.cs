using System.Linq;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;


namespace Valhalla.Validation.Editor.Tests
{
	[TestFixture]
	[Category("Content")]
	public class TestAssetsValidation
	{
		private static IValidatable[] ValidatableAssets
		{
			get
			{
				IValidatable[] validatableAssets = AssetDatabase
					.FindAssets($"t: {nameof(ScriptableObject)}")
					.Select(AssetDatabase.GUIDToAssetPath)
					.Select(AssetDatabase.LoadAssetAtPath<ScriptableObject>)
					.OfType<IValidatable>()
					.ToArray();

				if (validatableAssets.Length == 0)
					return new []{(IValidatable)null};

				return validatableAssets;
			}
		}



		[TestCaseSource(nameof(ValidatableAssets))]
		public void TestIsAssetValid(IValidatable asset)
		{
			if (asset == null)
			{
				Assert.Pass("No validatable assets found.");
				return;
			}
			
			var validationResult = asset.Validate();

			Assert.IsFalse(validationResult.IsInvalid, validationResult.GetCombinedError());
		}
	}
}
